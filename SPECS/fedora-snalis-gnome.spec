Name: fedalis-gnome
Version:	1
Release:	alpha%{?dist}
Summary: Everything for Gnome

License:	GPLv2
URL: http://snalis.org

Requires: gnome-tweak-tool
Requires: gnome-disk-utility

BuildArch: noarch

%description

%files


%changelog
* Fri Oct 23 2015 Crazyusb <ben@snalis.org>
- Creating Gnome
