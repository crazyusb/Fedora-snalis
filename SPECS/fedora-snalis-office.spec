Name: fedalis-office
Version:	1
Release:	beta%{?dist}
Summary: Office tools
Group:                  System Environment/Base

License:	GPLv2
URL: http://snalis.org

Requires: libreoffice
Requires: sil-andika-fonts
Requires: evince
Requires: calibre
Requires: fbreader
Requires: fbreader-gtk
Requires: simple-scan
Requires: xsane
Requires: verbiste
Requires: stardict-xmllittre
Requires: gimagereader-gtk
Requires: hack-fonts
BuildArch: noarch

%description
Office tools, and ebook management
%files

%changelog
* Wed Oct 21 2015 Crazyusb <ben@snalis.org>
- A start
