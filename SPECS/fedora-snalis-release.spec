Name:	fedalis-release
Version:	1
Release:	beta%{?dist}
Summary:	The Release for Fedalis

Group:                  System Environment/Base
License:	GPLv2
URL:		snalis.org

#Requires: fedalis-Build
Requires: rpmfusion-nonfree-release
Requires: rpmfusion-free-release
Requires: fedalis-repo
Requires: fedalis-office
Requires: fedalis-utility
Requires: fedalis-lang
Requires: fedalis-devel
Requires: fedalis-network
#Requires: fedalis-multimedia
Requires: fedalis-infographics
Requires: du-libre-repo

BuildArch: noarch
%description
Installing every package for Fedalis

%install
%{__install} -d -m755 %{buildroot}/etc
echo "Fedalis release %{version} %{release}" > %{buildroot}/etc/fedalis-release

%files
%config %attr(0644,root,root) /etc/fedalis-release

%changelog
* Fri Oct 21 2015 Crazyusb <ben@snalis.org> - 0.0.1-alpha
- Adding rpmfusion
* Wed Oct 21 2015 Crazyusb <ben@snalis.org> - 0.0.1-alpha
- Creating the first release package
