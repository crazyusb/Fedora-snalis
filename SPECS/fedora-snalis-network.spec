Name: fedalis-network
Version:	1
Release:	alpha%{?dist}
Summary: The network tools
Group:                  System Environment/Base

License:	GPLv2
URL: http://snalis.org

Requires: sshfs
Requires: chromium
Requires: firefox
Requires: liferea
Requires: transmission-gtk
Requires: libsmbclient
Requires: gvfs-smb
Requires: gnome-vfs2-smb
Requires: gigolo
Requires: filezilla
BuildArch: noarch

%description
Some useful soft for network use
%files

%changelog
* Wed Oct 21 2015 Crazyusb <ben@snalis.org>
- some tools
