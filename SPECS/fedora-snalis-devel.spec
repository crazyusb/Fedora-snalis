Name:		fedalis-devel
Version:	1
Release:	beta%{?dist}
Summary: Developping modules
Group:                  System Environment/Base
License:	GPLv2
URL:		snalis.org

#Requires: atom
Requires: livecd-tools
Requires: spin-kickstarts
Requires: pungi
Requires: fedora-kickstarts
Requires: mock
Requires: l10n-kickstarts
Requires: git
Requires: vim
Requires: rpm-build
Requires: rpmlint
Requires: rpmdevtools

BuildArch: noarch

%description
Because we do FLOSS, we need to distribuate the tools for adding, moding or rebulding Fedalis
%files

%changelog
* Tue Nov 03 2015 Crazyusb <ben@snalis.org>
- remove atom
* Fri Oct 23 2015 Crazyusb <ben@snalis.org> - 0.0.1-alpha
- Correcting some missed packages
* Wed Oct 21 2015 Crazyusb <ben@snalis.org> - 0.0.1-alpha
- I have started the base for developping tools, it the really beginning :)
