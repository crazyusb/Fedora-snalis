Name:		fedalis-infographics
Version:	1
Release:	alpha%{?dist}
Summary:	Everything that we need for the solid base of our OS
Group:                  System Environment/Base
License:	GPLv2
URL:		snalis.org

Requires: gimp
Requires: inkscape
Requires: shotwell
Requires: gthumb

BuildArch: noarch

%description
If you need to work or viewing picture, with this package you have everything
%files

%changelog
* Wed Oct 21 2015 Crazyusb <ben@snalis.org>
- The minimal
