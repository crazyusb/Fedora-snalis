Name:		fedalis-utility
Version:	1.1
Release:	beta%{?dist}
Summary:	Useful acessories for Fedalis and every sysadmin

Group:                  System Environment/Base

License:	GPLv2
URL:		snalis.org

Requires: htop
Requires: vim
Requires: screen
Requires: terminator
Requires: yumex-dnf
Requires: filezilla
Requires: gigolo
Requires: gparted
Requires: testdisk
Requires: wine
Requires: system-config-users
Requires: gnome-disk-utility
BuildArch: noarch

%description
Sysadmin tools box, not everythings but its a start
%files

%changelog
* Tue Oct 27 2015 Crazyusb <ben@snalis.org>
- Adding wine
* Fri Oct 23 2015 Crazyusb <ben@snalis.org>
- Adding gparted
* Wed Oct 21 2015 Crazyusb <ben@snalis.org>
- A start
