Name: fedalis-multimedia
Version:	1
Release:	beta%{?dist}
Summary: For multimedia use
Group:                  System Environment/Base
License:	GPLv2
URL: http://snalis.org

Requires: vlc
Requires: mplayer
Requires: x265
Requires: soundconverter
Requires: xfburn
Requires: brasero
Requires: pitivi
Requires: lmms
Requires: radiotray
Requires: asunder
Requires: smplayer
Requires:audacity
Requires: xvidcore
Requires: gstreamer-plugins-ugly
#Requires: libdvdcss
BuildArch: noarch

%description
%{Summary}
#If you want to watch a movies, listen radio or burning a cd, with this package you will have everything
%files

%changelog
* Wed Oct 21 2015 Crazyusb <ben@snalis.org>
- I put some codecs for now, if i find other codecs that useful I will add them
