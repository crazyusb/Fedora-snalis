Name:		fedalis-repo
Version:	1
Release:	1%{?dist}
Summary:	The repo
Group:                  System Environment/Base
License:	GPLv2
URL:		http://repo.du-libre.bzh/fedalis
BuildArch: noarch

%description
The repo for Fedalis

%install
%{__install} -d -m755 %{buildroot}%{_sysconfdir}/yum.repos.d
cat << EOF >>%{buildroot}%{_sysconfdir}/yum.repos.d/fedalis.repo
[fedalis]
name=Repository for Fedalis.
baseurl=http://repo.du-libre.bzh/fedalis/\$releasever/
enabled=1
skip_if_unavailable=1
metadata_expire=1d
gpgcheck=0
EOF



%files
%defattr(-,root,root)
%config %{_sysconfdir}/yum.repos.d/fedalis.repo

%changelog
*Fri Oct 23 2015 Crazyusb <ben@snalis.org> - 0.0.1-alpha
- Adding Fedalis repo
* Wed Oct 21 2015 Crazyusb <ben@snalis.org> - 0.0.1-alpha
- Creating the packages for the repo
