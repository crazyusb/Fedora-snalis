Name: fedalis-lang
Version:	1
Release:	alpha%{?dist}
Summary: All the language packages for Fedalis
Group:                  System Environment/Base
License:	GPLv2
URL: http://snalis.org

Requires: libreoffice-langpack-fr
Requires: hunspell-fr
Requires: aspell-fr
Requires: gimp-help-fr
Requires: man-pages-fr
Requires: gnome-getting-started-docs-fr

BuildArch: noarch

%description
If you have French speaker
%files

%changelog
* Wed Oct 21 2015 Crazyusb <ben@snalis.org>
- language for french speaker
