%packages
# Repo
rpmfusion-free-release
rpmfusion-nonfree-release
# Filesystem
gvfs-mtp
gvfs-smb
simple-mtpfs
xfsprogs
# Utility
yumex-dnf
nautilus-image-converter
unetbootin
gparted
gnome-disk-utility
strongswan
system-config-users
pulseaudio-module-bluetooth
webkitgtk4
wget
fedy
vim
htop
screen
nautilus-extensions
deja-dup-nautilus
brasero-nautilus
gutenprint-cups
hplip-gui
hplip-common
ndiswrapper
gnome-tweak-tool
xfce4-screenshooter
pyrenamer
# Virtual Keyboard
florence
# Screensaver
xscreensaver-base
xscreensaver-extras-base
xscreensaver-gl-base
xscreensaver-gl-extras
#System
wine
deja-dup

%end
