%include /home/ben/SNALIS/Fedora-snalis/fedora-live-workstation.ks
%include /home/ben/SNALIS/Fedora-snalis/fedora-snalis-repo.ks

%packages
@base-x
@core
@fonts
@guest-desktop-agents
@hardware-support
@multimedia
@networkmanager-submodules
@printing




#Users Software
gthumb


childsplay
childsplay-alphabet_sounds_fr
gcompris
gcompris-sound-fr
tuxmath
tuxtype2
tuxpaint




#SIP/XMPP
pidgin
pidgin-guifications
pidgin-musictracker
pidgin-otr
pidgin-privacy-please
pidgin-sipe



livecd-tools
spin-kickstarts
fedora-kickstarts
l10n-kickstarts
atom

gimp
inkscape
shotwell
gthumb



#Langue
libreoffice-langpack-fr
aspell-fr
hunspell-fr
gimp-help-fr



#Codec
#x264
soundconverter
xvidcore
vlc
audacious
audacity
clementine
smplayer
# winff # add http://winff.org/docs/rpm/fedora/winff-repo-1.0-1.noarch.rpm
# handbrake http://www.rpmfind.net/linux/RPM/sourceforge/s/sn/snowbird/yum/sb20/HandBrake-gui-0.9.9-12.fc20.x86_64.html
asunder
pitivi
xfburn
brasero
lmms
cheese
#smtube (compile it smtube.org)
#freetuxtv (freetuxtv on compile)
radiotray
#Popcorn time ?



chromium
liferea
transmission-gtk
filezilla
gigolo
#Skype ?
#jitsi.org ?
#Flash ?



# Libreoffice Suite
@libreoffice


#Font
sil-andika-fonts

#Mail client
# thunderbird (if XFCE)

#Scan
simple-scan
xsane

#Reader
evince
calibre
fbreader
fbreader-gtk

#OCR
gimagereader-gtk

#Dict
stardict-xmllittre
verbiste
# Grammalect ?
# Repo
rpmfusion-free-release
rpmfusion-nonfree-release
# Filesystem
gvfs-mtp
gvfs-smb
simple-mtpfs
xfsprogs
# Utility
yumex-dnf
nautilus-image-converter
unetbootin
gparted
gnome-disk-utility
strongswan
system-config-users
pulseaudio-module-bluetooth
webkitgtk4
wget
fedy
vim
htop
screen
nautilus-extensions
deja-dup-nautilus
brasero-nautilus
gutenprint-cups
hplip-gui
hplip-common
ndiswrapper
gnome-tweak-tool
xfce4-screenshooter
pyrenamer
# Virtual Keyboard
florence
# Screensaver
xscreensaver-base
xscreensaver-extras-base
xscreensaver-gl-base
xscreensaver-gl-extras
#System
#wine
deja-dup
%end
