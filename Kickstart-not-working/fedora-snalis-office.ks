%packages
# Libreoffice Suite
@libreoffice


#Font
sil-andika-fonts

#Mail client

#Scan
simple-scan
xsane

#Reader
evince
calibre
fbreader
fbreader-gtk

#OCR
gimagereader-gtk

#Dict
stardict-xmllittre
verbiste


%end
