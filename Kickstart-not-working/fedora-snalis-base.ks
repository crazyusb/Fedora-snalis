%include fedora-snalis-repo.ks
%include fedora-live-base.ks
%include fedora-snalis-language.ks
%include fedora-snalis-utility.ks
%include fedora-snalis-multimedia.ks
%include fedora-snalis-networking.ks
%include fedora-snalis-office.ks
%include fedora-snalis-communication.ks
%include fedora-snalis-development.ks

%packages

@base-x
@core
@fonts
@guest-desktop-agents
@hardware-support
@multimedia
@networkmanager-submodules
@printing




#Users Software
gthumb

%end
