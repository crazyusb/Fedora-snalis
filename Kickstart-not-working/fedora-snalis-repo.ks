# REPOS

# KP - production repositories
#repo --name="Adobe Systems Incorporated" --baseurl=http://linuxdownload.adobe.com/linux/$basearch/ --cost=1000
# We need 32bit for 64bit images also
#repo --name="Adobe Systems Incorporated - 32bit" --baseurl=http://linuxdownload.adobe.com/linux/i386/ --cost=1000

repo --name="Fedora $releasever - $basearch" --baseurl=http://dl.fedoraproject.org/pub/fedora/linux/releases/$releasever/Everything/$basearch/os/ --cost=1000
repo --name="Fedora $releasever - $basearch - Updates" --baseurl=http://dl.fedoraproject.org/pub/fedora/linux/updates/$releasever/$basearch/ --cost=1000
#repo --name="Fedora $releasever - $basearch - Updates Testing" --baseurl=http://download.fedoraproject.org/pub/fedora/linux/updates/testing/$releasever/$basearch/ --cost=1000

# Google Chrome
#repo --name="Chromium" --baseurl=https://repos.fedorapeople.org/repos/spot/chromium-stable/fedora-$releasever/$basearch/
#repo --name="Google Chrome" --baseurl=http://dl.google.com/linux/chrome/rpm/stable/$basearch/ --cost=1000

# KORORA REPOS, set to remote for release, local for testing
#repo --name="Korora $releasever" --baseurl=http://dl.kororaproject.org/pub/korora/releases/$releasever/$basearch/ --cost=100
#repo --name="Korora $releasever - Testing" --baseurl=http://dl.kororaproject.org/pub/korora/testing/$releasever/$basearch/ --cost=100

repo --name="RPMFusion Free" --baseurl=http://download1.rpmfusion.org/free/fedora/releases/$releasever/Everything/$basearch/os/ --cost=1000
repo --name="RPMFusion Free - Updates" --baseurl=http://download1.rpmfusion.org/free/fedora/updates/$releasever/$basearch/ --cost=1000

repo --name="RPMFusion Non-Free" --baseurl=http://download1.rpmfusion.org/nonfree/fedora/releases/$releasever/Everything/$basearch/os/ --cost=1000
repo --name="RPMFusion Non-Free - Updates" --baseurl=http://download1.rpmfusion.org/nonfree/fedora/updates/$releasever/$basearch/ --cost=1000
#repo --name="OzonOS" --baseurl=http://goodies.ozon-os.com/repo/$releasever/

#repo --name="VirtualBox" --baseurl=http://download.virtualbox.org/virtualbox/rpm/fedora/$releasever/$basearch/ --cost=1000

#
# KP - development repositories
#repo --name="Fedora $releasever - $basearch" --baseurl=http://dl.fedoraproject.org/pub/fedora/linux/testing/$releasever/Everything/%KP_BASEARCH%%/os/ --cost=1000
#repo --name="Fedora $releasever - $basearch Updates Released" --baseurl=http://dl.fedoraproject.org/pub/fedora/linux/updates/$releasever/$basearch/ --cost=1000
#repo --name="Fedora $releasever - $basearch Updates Testing" --baseurl=http://dl.fedoraproject.org/pub/fedora/linux/updates/testing/$releasever/$basearch/ --cost=1000

#repo --name="RPMFusion Free - Development" --baseurl=http://download1.rpmfusion.org/free/fedora/development/$releasever/$basearch/os/ --cost=1000
#repo --name="RPMFusion Non-Free - Development" --baseurl=http://download1.rpmfusion.org/nonfree/fedora/development/$releasever/$basearch/os/ --cost=1000
# RAWHIDE - use when RPM Fusion has not yet branched (usually because fedora is still pre-beta)
#repo --name="RPMFusion Free - Development" --baseurl=http://download1.rpmfusion.org/free/fedora/development/rawhide/$basearch/os/ --cost=1000
#repo --name="RPMFusion Non-Free - Development" --baseurl=http://download1.rpmfusion.org/nonfree/fedora/development/rawhide/$basearch/os/ --cost=1000

#Snalis repo
repo --name="Du-libre.bzh" --baseurl=http://repo.du-libre.bzh/
