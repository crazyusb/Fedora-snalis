%include fedora-livecd-xfce.ks

repo --name="RPMFusion Free" --baseurl=http://download1.rpmfusion.org/free/fedora/releases/$releasever/Everything/$basearch/os/ --cost=1000
repo --name="RPMFusion Free - Updates" --baseurl=http://download1.rpmfusion.org/free/fedora/updates/$releasever/$basearch/ --cost=1000

repo --name="RPMFusion Non-Free" --baseurl=http://download1.rpmfusion.org/nonfree/fedora/releases/$releasever/Everything/$basearch/os/ --cost=1000
repo --name="RPMFusion Non-Free - Updates" --baseurl=http://download1.rpmfusion.org/nonfree/fedora/updates/$releasever/$basearch/ --cost=1000
repo --name="Du-libre" --baseurl=http://repo.du-libre.bzh/fedora/linux/releases/$releasever/
repo --name="Fedalis" --baseurl=http://repo.du-libre.bzh/fedalis/$releasever/


%packages
fedalis-release
fedalis-settings-xfce
%end
